public class Main {
    public static void main(String[] args) {
        Task evenNumbersPrintTask = new EvenNumbersPrintTask(0, 14);
        Task oddNumbersPrintTask = new OddNumbersPrintTask(0, 14);

        evenNumbersPrintTask.complete();

        oddNumbersPrintTask.complete();
    }

}