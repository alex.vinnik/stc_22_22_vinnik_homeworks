INSERT INTO public.users
(first_name, surname, phone_number, experience, age, has_license, category_rights, rating)
VALUES('German', 'Harber', '985-011-10-87', 4, 22, true, 'B', 5),
('Dan', 'Cole', '903-582-86-66', 10, 28, true, 'B', 1),
('Helga', 'Fay', '917-510-02-05', 14, 32, true, 'B', 2),
('Shea', 'Harber', '916-688-86-14', 6, 24, true, 'B', 3),
('Harber', 'Fay', '966-058-78-87', 2, 20, true, 'B', 4);

INSERT INTO public.car
(model, color, number_car, users_id)
VALUES('Acura', 'White', 'см678к159', 1),
('Aston Martin', 'Red', 'см608к59', 2),
('Buick', 'Blue', 'со178а159', 3),
('Chery', 'Black', 'аа678а159', 4),
('BMW', 'White', 'оо777о159', 5);

INSERT INTO public.the_trip
(users_id, car_id, trip_date, trip_duration)
VALUES(1, 1, '2022-11-13', 10),
(2, 2, '2022-11-13', 24),
(3, 3, '2022-11-13', 4),
(4, 4, '2022-11-13', 1),
(5, 5, '2022-11-13', 6);