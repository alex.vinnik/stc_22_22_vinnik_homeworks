drop table if exists the_trip;
drop table if exists car;
drop table if exists users;

create table users (
	id bigserial primary key,
	first_name varchar(50) not null,
	surname varchar(50) not null,
	phone_number varchar(20) not null,
	experience integer check (experience >= 0 and experience <= 120) not null default 0,
	age integer check (age >= 0 and age <= 120) not null,
	has_license boolean not null,
	category_rights varchar(3) not null,
	rating real check (rating >= 0 and rating <= 5) not null default 0
);

create table car (
	id bigserial primary key,
	model varchar(50) not null,
	color varchar(100) not null,
	number_car varchar(20) not null,
	users_id integer not null references users(id) 
);

create table the_trip (
	id bigserial primary key,
	users_id integer not null references users(id),
	car_id integer not null references car(id),
	trip_date date not null,
	trip_duration integer not null
);