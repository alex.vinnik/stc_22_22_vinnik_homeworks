import java.util.Arrays;
import java.util.OptionalInt;

public class Main {
    public static void main(String[] args) {

        int[] array = {10, 4, 15, 66, 77, 11, 22};
        int start = 1;
        int end = 4;

        ArrayTask amountGivenInterval = (arr, from, to) -> {
            int sumElement = 0;
            from = (from < 1) ? 1 : from;
            to = (to >= array.length) ? array.length : to;
            for (int i = from - 1; i < to; i++) {
                sumElement += arr[i];
            }
            return sumElement;
        };

        ArrayTask sumDigitsMaximum = (arr, from, to) -> {
            int sumDigits = 0;
            int max = 0;

            from = (from < 1) ? 0 : from;
            to = (to >= array.length) ? array.length : to;

            for (int i = from - 1; i < to; i++) {
                max = (arr[i] > max) ? arr[i] : max;
            }

            for (int i = max; i != 0; i /= 10) {
                sumDigits += (i % 10);
            }
            return sumDigits;
        };

        System.out.println(amountGivenInterval.resolve(array, start, end));
        System.out.println(sumDigitsMaximum.resolve(array, start, end));

        ArraysTasksResolver.resolveTask(array, amountGivenInterval, start, end);
        ArraysTasksResolver.resolveTask(array, sumDigitsMaximum, start, end);
    }
}