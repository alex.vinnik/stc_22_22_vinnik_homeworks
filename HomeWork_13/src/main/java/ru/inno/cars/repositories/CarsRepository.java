package ru.inno.cars.repositories;

import ru.inno.cars.models.Car;

import java.util.List;

public interface CarsRepository {
    List<Car> findAll();

    void save(Car car);
}
