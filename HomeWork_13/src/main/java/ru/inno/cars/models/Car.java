package ru.inno.cars.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Car {

    private Long id;
    private String model;
    private String number_car;
    private String color;
}
