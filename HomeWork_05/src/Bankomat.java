public class Bankomat {
    private static final double MAX_VOLUME_MONEY = 1000;
    private static final double MAX_SUM_RAZRESH_VYD = 300;
    private double remainingMoney;
    private double maxSumRazreshVyd;
    private double maxVolumeMoney;
    private double countProvOperation;

    public Bankomat() {
        this.countProvOperation = 0;
        this.maxSumRazreshVyd = MAX_SUM_RAZRESH_VYD;
        this.maxVolumeMoney = MAX_VOLUME_MONEY;
        this.remainingMoney = MAX_VOLUME_MONEY;
    }

    public Bankomat(double maxSumRazreshVyd, double maxVolumeMoney) {
        this.countProvOperation = 0;
        this.maxSumRazreshVyd = maxSumRazreshVyd;
        this.maxVolumeMoney = maxVolumeMoney;
        this.remainingMoney = maxVolumeMoney;
    }

    public Bankomat(double maxSumRazreshVyd, double maxVolumeMoney, double remainingMoney) {
        this(maxSumRazreshVyd, maxVolumeMoney);
        this.remainingMoney = remainingMoney;
    }

    public Bankomat(double remainingMoney) {
        this();
        this.remainingMoney = remainingMoney;
    }

    public double giveMoney(double sumMoney) {
        this.countProvOperation++;
        if (this.remainingMoney == 0)
            return 0;

        if (sumMoney <= this.maxSumRazreshVyd) {
            if (sumMoney <= this.remainingMoney) {
                this.remainingMoney -= sumMoney;
                return sumMoney;
            } else {
                double tmpRemainingMoney = this.remainingMoney;
                this.remainingMoney -= tmpRemainingMoney;
                return tmpRemainingMoney;
            }
        } else {
            this.remainingMoney -= this.maxSumRazreshVyd;
            return this.maxSumRazreshVyd;
        }
    }

    public double putMoney(double sumMoney) {
        this.countProvOperation++;
        if (sumMoney != 0) {
            double sumPutMoney = this.remainingMoney + sumMoney;
            if (sumPutMoney > this.maxVolumeMoney) {
                this.remainingMoney = this.maxVolumeMoney;
                return sumPutMoney - this.maxVolumeMoney;
            } else {
                this.remainingMoney = sumPutMoney;
                return 0;
            }
        }
        return 0;
    }
}
