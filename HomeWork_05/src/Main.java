public class Main {
    public static void main(String[] args) {
        Bankomat ATM = new Bankomat();

        assert ATM.putMoney(1000) == 1000 : " != 1000";
        assert ATM.giveMoney(400) == 300: " != 300";
        assert ATM.giveMoney(300) == 300: " != 300";
        assert ATM.giveMoney(200) != 0: " == 0";
        assert ATM.giveMoney(300) == 200: " != 200";
        assert ATM.giveMoney(300) == 0: " != 0";
    }
}