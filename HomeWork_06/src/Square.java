public class Square extends Figure {
    private static final double SIDE_LENGTH_DEF = 1;

    public void setSideLength(double sideLength) {
        this.sideLength = sideLength;
    }

    private double sideLength;

    public double getSideLength() {
        return sideLength;
    }

    public Square() {
        super();
        this.sideLength = SIDE_LENGTH_DEF;
    }

    public Square(double x, double y) {
        super(x, y);
        this.sideLength = SIDE_LENGTH_DEF;
    }

    public Square(double x, double y, double sideLength) {
        super(x, y);
        this.sideLength = sideLength;
    }

    public Square(double sideLength) {
        super();
        this.sideLength = sideLength;
    }

    public double perimeter() {
        return 4 * sideLength;
    }

    public double area() {
        return this.sideLength * this.sideLength;
    }
}
