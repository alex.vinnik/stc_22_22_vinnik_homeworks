public class Ellipse extends Circle {
    private static final double LARGE_RADIUS_DEF = 2;

    public void setLargeRadius(double largeRadius) {
        this.largeRadius = largeRadius;
    }

    private double largeRadius;

    public Ellipse() {
        super();
        this.largeRadius = LARGE_RADIUS_DEF;
    }

    public Ellipse(double x, double y) {
        super(x, y);
        this.largeRadius = LARGE_RADIUS_DEF;
    }

    public Ellipse(double largeRadius) {
        super();
        this.largeRadius = largeRadius;
    }

    public Ellipse(double x, double y, double smallRadius, double largeRadius) {
        super(x, y, smallRadius);
        this.largeRadius = largeRadius;
    }

    @Override
    public double perimeter() {
        double sRadius = getRadius() * getRadius();
        double lRadius = this.largeRadius * this.largeRadius;
        return 2 * Math.PI * Math.sqrt((sRadius + lRadius) / 2);
    }

    @Override
    public double area() {
        return Math.PI * this.largeRadius * getRadius();
    }
}
