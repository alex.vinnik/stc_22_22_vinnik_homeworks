public class Rectangle extends Figure {
    private static final double HEIGHT_DEF = 1;
    private static final double WIDTH_DEF = 2;

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    private double height;
    private double width;

    public Rectangle() {
        super();
        this.height = HEIGHT_DEF;
        this.width = WIDTH_DEF;
    }

    public Rectangle(double x, double y, double height, double width) {
        super(x, y);
        this.height = height;
        this.width = width;
    }

    public Rectangle(double height, double width) {
        super();
        this.height = height;
        this.width = width;
    }

    public double perimeter() {
        return 2 * (this.height + this.width);
    }

    public double area() {
        return this.height * this.width;
    }
}
