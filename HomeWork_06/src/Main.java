import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        Circle c = new Circle();
        Ellipse elp = new Ellipse();
        Rectangle rect = new Rectangle();
        Square sq = new Square();

        c.setX(2);
        c.setY(3);
        c.setRadius(1);

        double per = dRound2(c.perimeter());
        double ar = dRound2(c.area());

        assert per == 6.28 : "perC != actual";
        assert ar == 3.14 : "arC != actual";

        c.move(3, 5);
        assert c.getX() == 5 : "xC != actual";
        assert c.getY() == 8 : "yC != actual";

        elp.setX(2);
        elp.setY(3);
        elp.setRadius(1);
        elp.setLargeRadius(2);

        double perE = dRound2(elp.perimeter());
        double arE = dRound2(elp.area());

        assert perE == 9.93 : "perE != actual";
        assert arE == 6.28 : "arE != actual";

        elp.move(3, 5);
        assert elp.getX() == 5 : "xE != actual";
        assert elp.getY() == 8 : "yE != actual";

        rect.setX(2);
        rect.setY(3);
        rect.setWidth(2);
        rect.setHeight(4);

        double perR = dRound2(rect.perimeter());
        double arR = dRound2(rect.area());

        assert perR == 12.0 : "perR != actual";
        assert arR == 8.0 : "arR != actual";

        rect.move(3, 5);
        assert rect.getX() == 5 : "xR != actual";
        assert rect.getY() == 8 : "yR != actual";

        sq.setX(2);
        sq.setY(3);
        sq.setSideLength(1);

        double perS = dRound2(sq.perimeter());
        double arS = dRound2(sq.area());

        assert perS == 4.0 : "perS != actual";
        assert arS == 1.0 : "arS != actual";

        sq.move(3, 5);
        assert sq.getX() == 5 : "xS != actual";
        assert sq.getY() == 8 : "yS != actual";
    }

    public static double dRound2(double cr) {
        return BigDecimal.valueOf(cr).setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue();
    }
}