public class Figure {
    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    private double x;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    private double y;

    public Figure() {
        this.x = 0;
        this.y = 0;
    }

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void move(double x, double y){
        this.x += x;
        this.y += y;
    }
}
