public class Circle extends Figure {
    private static final double RADIUS_DEF = 1;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    private double radius;

    public Circle() {
        super();
        this.radius = RADIUS_DEF;
    }

    public Circle(double radius) {
        super();
        this.radius = radius;
    }

    public Circle(double x, double y) {
        super(x, y);
        this.radius = RADIUS_DEF;
    }

    public Circle(double x, double y, double radius) {
        super(x, y);
        this.radius = radius;
    }

    public double perimeter() {
        return 2 * this.radius * Math.PI;
    }

    public double area() {
        return Math.PI * (this.radius * this.radius);
    }
}
