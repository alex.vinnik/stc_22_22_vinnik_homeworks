import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Введите кол-во элементов массива: ");
        int number = scan.nextInt();
        int countLocMin = 0;
        int[] array = new int[number];
        //Ввовд элементов массива с клавиатуры
        for (int i = 0; i < number; i++) {
            System.out.print("Введите значение " + i + " элемента массива: ");
            array[i] = scan.nextInt();
        }
        //Проверяем если 1 элемент массива меньше 2 тогда нашли кол-во локальных мин увеличиваем на 1
        if (array[0] < array[1]) {
            countLocMin++;
        }
        //Обход всего массива
        for (int i = 1; i < number - 1; i++) {
            //Поиск локального минимума
            if (array[i - 1] > array[i] && array[i] < array[i + 1]) {
                countLocMin++;//Если нашли, тогда увеличиваем счетчик на 1
            }
        }
        //Проверяем меньши ли последний элемент массива в сравнение с предпоследним
        if (array[number - 2] > array[number - 1]) {
            countLocMin++;
        }
        //Выводим длину массива, элементы массива и кол-во минимумов
        System.out.println(array.length);
        System.out.println(Arrays.toString(array));
        System.out.println(countLocMin);
    }
}