public class LinkedList<T> implements List<T> {

    private Node<T> first;

    private Node<T> last;
    private int count;

    private static class Node<E> {
        E value;
        Node<E> next;

        public Node(E value) {
            this.value = value;
        }
    }

    @Override
    public void add(T element) {
        // создаем новый узел со значением
        Node<T> newNode = new Node<>(element);
        // если элементов в списке нет
        if (count == 0) {
            // кладем новый узел в качестве первого и последнего
            this.first = newNode;
        } else {
            // если в списке уже есть узлы
            // у последнего делаем следующим новый узел
            this.last.next = newNode;
        }
        this.last = newNode;
        count++;
    }

    @Override
    public void remove(T element) {
        Node<T> previous = null;
        Node<T> current = first;

        while (current != null) {
            if (current.value.equals(element)) {
                deleteElement((Node<T>) previous, (Node<T>) current);
                break;
            }

            previous = current;
            current = current.next;
        }
    }

    @Override
    public boolean contains(T element) {
        // получить ссылку на первый элемент списка
        // пройтись по всем элементам списка
        Node<T> current = this.first;
        // пока не обошли весь список
        while (current != null) {
            // если значение в текущем узле совпало с искомым
            if (current.value.equals(element)) {
                return true;
            }
            // если не совпало - идем к следующему узлу
            current = current.next;
        }
        // если не нашли элемент
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {
        if (0 <= index && index <= count) {
            Node<T> previous = null;
            Node<T> current = this.first;

            for (int i = 0; i < size(); i++) {
                if (i == index) {
                    deleteElement((Node<T>) previous, (Node<T>) current);
                    break;
                }

                previous = current;
                current = current.next;
            }

        }
    }

    private void deleteElement(Node<T> previous, Node<T> current) {
        if (previous != null) {
            previous.next = current.next;
            if (current.next == null) {
                last = previous;
            }
        } else {
            first = first.next;

            if (first == null) {
                last = null;
            }
        }
        count--;
    }

    @Override
    public T get(int index) {
        if (0 <= index && index <= count) {
            // начинаем с первого элемента
            Node<T> current = this.first;

            // если запрошенный индекс был равен - 3
            // 0, 1, 2
            for (int i = 0; i < index; i++) {
                // на каждом шаге цикла двигаемся дальше
                current = current.next;
            }
            return current.value;
        }
        return null;

    }

    // внутренний
    private class LinkedListIterator implements Iterator<T> {

        private Node<T> current = first;

        @Override
        public T next() {
            // берем и запоминаем значение текущего узла
            T value = current.value;
            // сдвигаем current на следующий узел
            current = current.next;
            // возвращаем значение узла
            return value;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }
}
