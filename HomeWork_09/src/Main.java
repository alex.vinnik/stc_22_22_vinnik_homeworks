//import java.util.LinkedList;
//import java.util.ListIterator;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> resultArray = new ArrayList<Integer>();

        resultArray.add(2);
        resultArray.add(3);

        ArrayList<Integer> testArray = new ArrayList<Integer>();

        testArray.add(1);
        testArray.add(2);
        testArray.add(3);
        testArray.add(4);

        testArray.remove(23);
        testArray.removeAt(2);

        assert  resultArray.size() == testArray.size() : "resultArray.size() != testArray.size()";
        for (int i = 0; i < resultArray.size(); i++) {
            assert  resultArray.get(i) == testArray.get(i) : "resultArray.get(i) != testArray.get(i)";
        }

        LinkedList<Integer> resultList = new LinkedList<Integer>();

        resultList.add(2);
        resultList.add(3);

        LinkedList<Integer> testList = new LinkedList<Integer>();

        testList.add(1);
        testList.add(2);
        testList.add(3);
        testList.add(4);

        testList.remove(1);
        testList.removeAt(2);

        assert  resultList.size() == testList.size() : "resultList.size() != testList.size()";
        for (int i = 0; i < resultList.size(); i++) {
            assert  resultList.get(i) == testList.get(i) : "resultList.get(i) != testList.get(i)";
        }
    }
}