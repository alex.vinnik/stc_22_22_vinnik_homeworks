package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.MovieForm;
import ru.inno.ec.models.Genre;
import ru.inno.ec.models.Movie;
import ru.inno.ec.repositories.GenresRepository;
import ru.inno.ec.repositories.MovieRepository;
import ru.inno.ec.services.MovieService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;

    private final GenresRepository genresRepository;

    @Override
    public List<Movie> getAllMovies() {
        return movieRepository.findAllByStateNot(Movie.State.DELETED);
    }

    @Override
    public void addMovie(MovieForm movie, Long id) {
        Genre genre = genresRepository.findById(id).orElseThrow();
        Movie newMovie = Movie.builder()
                .nameMovie(movie.getNameMovie())
                .ageLimit(movie.getAgeLimit())
                .year(movie.getYear())
                .country(movie.getCountry())
                .genre(genre)
                .duration(movie.getDuration())
                .state(Movie.State.CONFIRMED)
                .build();

        movieRepository.save(newMovie);
    }

    @Override
    public Movie getMovie(Long movieId) {
        return movieRepository.findById(movieId).orElseThrow();
    }

    @Override
    public void updateMovie(Long movieId, Long genreId, MovieForm updateData) {
        Movie movieForUpdate = movieRepository.findById(movieId).orElseThrow();
        Genre genre = genresRepository.findById(genreId).orElseThrow();

        movieForUpdate.setNameMovie(updateData.getNameMovie());
        movieForUpdate.setYear(updateData.getYear());
        movieForUpdate.setCountry(updateData.getCountry());
        movieForUpdate.setAgeLimit(updateData.getAgeLimit());
        movieForUpdate.setDuration(updateData.getDuration());
        movieForUpdate.setGenre(genre);

        movieRepository.save(movieForUpdate);
    }

    @Override
    public void deleteMovie(Long movieId) {
        Movie movieForDelete = movieRepository.findById(movieId).orElseThrow();
        movieForDelete.setState(Movie.State.DELETED);

        movieRepository.save(movieForDelete);
    }
}
