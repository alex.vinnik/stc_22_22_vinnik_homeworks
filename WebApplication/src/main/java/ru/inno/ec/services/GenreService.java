package ru.inno.ec.services;

import ru.inno.ec.dto.GenreForm;
import ru.inno.ec.models.Genre;

import java.util.List;

public interface GenreService {
    List<Genre> getAllGenres();

    void addGenre(GenreForm genre);

    Genre getGenre(Long id);

    void updateGenre(Long genreId, GenreForm updateData);

    void deleteGenre(Long genreId);
}
