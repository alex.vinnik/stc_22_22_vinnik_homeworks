package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.ScheduleForm;
import ru.inno.ec.models.Genre;
import ru.inno.ec.models.Movie;
import ru.inno.ec.models.Schedule;
import ru.inno.ec.repositories.MovieRepository;
import ru.inno.ec.repositories.ScheduleRepository;
import ru.inno.ec.services.ScheduleService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ScheduleServiceImpl implements ScheduleService {

    private final ScheduleRepository scheduleRepository;

    private final MovieRepository movieRepository;
    @Override
    public List<Schedule> getAllSchedules() {
        return scheduleRepository.findAllByStateNot(Schedule.State.DELETED);
    }

    @Override
    public void addSchedule(ScheduleForm schedule, Long movieId) {
        Movie movie = movieRepository.findById(movieId).orElseThrow();
        Schedule newSchedule = Schedule.builder()
                .dateSession(schedule.getDateSession())
                .timeStart(schedule.getTimeStart().toString())
                .formMovie(schedule.getFormMovie())
                .price(schedule.getPrice())
                .hall(schedule.getHall())
                .movie(movie)
                .state(Schedule.State.CONFIRMED)
                .build();

        scheduleRepository.save(newSchedule);
    }

    @Override
    public Schedule getSchedule(Long scheduleId) {
        return scheduleRepository.findById(scheduleId).orElseThrow();
    }

    @Override
    public void updateSchedule(Long scheduleId, Long movieId, ScheduleForm updateData) {
        Schedule scheduleForUpdate = scheduleRepository.findById(scheduleId).orElseThrow();
        Movie movie = movieRepository.findById(movieId).orElseThrow();

        scheduleForUpdate.setDateSession(updateData.getDateSession());
        scheduleForUpdate.setTimeStart(updateData.getTimeStart().toString());
        scheduleForUpdate.setFormMovie(updateData.getFormMovie());
        scheduleForUpdate.setPrice(updateData.getPrice());
        scheduleForUpdate.setHall(updateData.getHall());
        scheduleForUpdate.setMovie(movie);

        scheduleRepository.save(scheduleForUpdate);
    }

    @Override
    public void deleteSchedule(Long scheduleId) {
        Schedule scheduleForDelete = scheduleRepository.findById(scheduleId).orElseThrow();
        scheduleForDelete.setState(Schedule.State.DELETED);

        scheduleRepository.save(scheduleForDelete);
    }
}
