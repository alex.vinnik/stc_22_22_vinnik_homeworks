package ru.inno.ec.services;

import ru.inno.ec.dto.ScheduleForm;
import ru.inno.ec.models.Schedule;

import java.util.List;

public interface ScheduleService {
    List<Schedule> getAllSchedules();

    void addSchedule(ScheduleForm schedule, Long movieId);

    Schedule getSchedule(Long scheduleId);

    void updateSchedule(Long scheduleId, Long movieId, ScheduleForm updateData);

    void deleteSchedule(Long scheduleId);
}
