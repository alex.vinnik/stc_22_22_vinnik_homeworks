package ru.inno.ec.services;

import ru.inno.ec.dto.MovieForm;
import ru.inno.ec.models.Genre;
import ru.inno.ec.models.Movie;
import ru.inno.ec.models.User;

import java.util.List;

public interface MovieService {
    List<Movie> getAllMovies();

    void addMovie(MovieForm movie,  Long id);

    Movie getMovie(Long movieId);

    void updateMovie(Long movieId, Long genreId, MovieForm updateData);

    void deleteMovie(Long movieId);
}
