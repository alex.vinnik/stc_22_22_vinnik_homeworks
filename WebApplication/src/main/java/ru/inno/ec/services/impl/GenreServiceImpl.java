package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.GenreForm;
import ru.inno.ec.models.Genre;
import ru.inno.ec.repositories.GenresRepository;
import ru.inno.ec.services.GenreService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GenreServiceImpl implements GenreService {

    private final GenresRepository genresRepository;

    @Override
    public List<Genre> getAllGenres() {
        return genresRepository.findAllByStateNot(Genre.State.DELETED);
    }

    @Override
    public void addGenre(GenreForm genre) {
        Genre newGenre = Genre.builder()
                .nameGenre(genre.getNameGenre())
                .state(Genre.State.CONFIRMED)
                .build();

        genresRepository.save(newGenre);
    }

    @Override
    public Genre getGenre(Long id) {
        return genresRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateGenre(Long genreId, GenreForm updateData) {
        Genre genreForUpdate = genresRepository.findById(genreId).orElseThrow();
        genreForUpdate.setNameGenre(updateData.getNameGenre());
        genresRepository.save(genreForUpdate);
    }

    @Override
    public void deleteGenre(Long genreId) {
        Genre userForDelete = genresRepository.findById(genreId).orElseThrow();
        userForDelete.setState(Genre.State.DELETED);

        genresRepository.save(userForDelete);
    }
}
