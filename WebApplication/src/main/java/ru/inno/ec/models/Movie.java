package ru.inno.ec.models;

import lombok.*;
import org.hibernate.annotations.Check;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = {"genre", "movies"})
@Table(name = "movie")
public class Movie {

    public enum State {
        CONFIRMED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name_movie")
    private String nameMovie;

    private String year;

    private String country;

    @Column(name = "age_limit")
    @Check(constraints = "age >= 0 and age <= 120")
    private Integer ageLimit;

    private Integer duration;

    @Enumerated(value = EnumType.STRING)
    private Movie.State state;

    @ManyToOne
    @JoinColumn(name = "genre_id")
    private Genre genre;

    @OneToMany(mappedBy = "movie", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Schedule> schedules;
}
