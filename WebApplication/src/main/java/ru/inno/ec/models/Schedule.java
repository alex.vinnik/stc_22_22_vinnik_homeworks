package ru.inno.ec.models;

import lombok.*;
import org.hibernate.annotations.Check;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "movie")
@Table(name = "schedule")
public class Schedule {

    public enum State {
        CONFIRMED, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_session")
    private Date dateSession;

    @Column(name = "time_start")
    private String timeStart;

    @Column(name = "form_movie")
    private String formMovie;

    private Double price;

    private String hall;

    @Enumerated(value = EnumType.STRING)
    private Schedule.State state;

    @ManyToOne
    @JoinColumn(name = "movie_id")
    private Movie movie;
}
