package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.ec.dto.GenreForm;
import ru.inno.ec.security.details.CustomUserDetails;
import ru.inno.ec.services.GenreService;

@RequiredArgsConstructor
@Controller
public class GenreController {
    private final GenreService genreService;

    @GetMapping("/genres")
    public String getGenresPage(@RequestParam(value = "orderBy", required = false) String orderBy,
                                @RequestParam(value = "dir", required = false) String direction,
                                @AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("genres", genreService.getAllGenres());
        return "genres/genres_page";
    }

    @PostMapping("/genres")
    public String addGenre(GenreForm genre) {
        genreService.addGenre(genre);
        return "redirect:/genres";
    }

    @GetMapping("/genres/{genre-id}")
    public String getGenrePage(@PathVariable("genre-id") Long genreId,
                               @AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("genre", genreService.getGenre(genreId));
        return "genres/genre_page";
    }

    @PostMapping("/genres/{genre-id}/update")
    public String updateGenre(@PathVariable("genre-id") Long genreId, GenreForm genre) {
        genreService.updateGenre(genreId, genre);
        return "redirect:/genres/" + genreId;
    }

    @GetMapping("/genres/{genre-id}/delete")
    public String updateGenre(@PathVariable("genre-id") Long genreId) {
        genreService.deleteGenre(genreId);
        return "redirect:/genres/";
    }
}
