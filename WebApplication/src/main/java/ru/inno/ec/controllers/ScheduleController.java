package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.ec.dto.MovieForm;
import ru.inno.ec.dto.ScheduleForm;
import ru.inno.ec.security.details.CustomUserDetails;
import ru.inno.ec.services.GenreService;
import ru.inno.ec.services.MovieService;
import ru.inno.ec.services.ScheduleService;

@RequiredArgsConstructor
@Controller
public class ScheduleController {
    private final MovieService movieService;
    private final ScheduleService scheduleService;

    @GetMapping("/schedules")
    public String getMoviesPage(@RequestParam(value = "orderBy", required = false) String orderBy,
                                @RequestParam(value = "dir", required = false) String direction,
                                @AuthenticationPrincipal CustomUserDetails customUserDetails,
                                Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("movies", movieService.getAllMovies());
        model.addAttribute("schedules", scheduleService.getAllSchedules());
        return "schedules/schedules_page";
    }

    @PostMapping("/schedules")
    public String addMovie(ScheduleForm schedule, @RequestParam("movie-id") Long id) {
        scheduleService.addSchedule(schedule, id);
        return "redirect:/schedules";
    }

    @GetMapping("/schedules/{schedule-id}")
    public String getMoviePage(@PathVariable("schedule-id") Long scheduleId,
                               @AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("schedule", scheduleService.getSchedule(scheduleId));
        model.addAttribute("movies", movieService.getAllMovies());
        return "schedules/schedule_page";
    }

    @PostMapping("/schedules/{schedule-id}/update")
    public String updateMovie(@PathVariable("schedule-id") Long scheduleId,
                              @RequestParam("movie-id") Long movieId, ScheduleForm schedule) {
        scheduleService.updateSchedule(scheduleId, movieId, schedule);
        return "redirect:/schedules/" + scheduleId;
    }

    @GetMapping("/schedules/{schedule-id}/delete")
    public String updateMovie(@PathVariable("schedule-id") Long scheduleId) {
        scheduleService.deleteSchedule(scheduleId);
        return "redirect:/schedules/";
    }
}
