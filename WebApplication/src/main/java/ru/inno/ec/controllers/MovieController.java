package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.ec.dto.MovieForm;
import ru.inno.ec.security.details.CustomUserDetails;
import ru.inno.ec.services.GenreService;
import ru.inno.ec.services.MovieService;

@RequiredArgsConstructor
@Controller
public class MovieController {

    private final MovieService movieService;
    private final GenreService genreService;

    @GetMapping("/movies")
    public String getMoviesPage(@RequestParam(value = "orderBy", required = false) String orderBy,
                                @RequestParam(value = "dir", required = false) String direction,
                                @AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("movies", movieService.getAllMovies());
        model.addAttribute("genres", genreService.getAllGenres());
        return "movies/movies_page";
    }

    @PostMapping("/movies")
    public String addMovie(MovieForm movie, @RequestParam("genre-id") Long id) {
        movieService.addMovie(movie, id);
        return "redirect:/movies";
    }

    @GetMapping("/movies/{movie-id}")
    public String getMoviePage(@PathVariable("movie-id") Long movieId,
                               @AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("genres", genreService.getAllGenres());
        model.addAttribute("movie", movieService.getMovie(movieId));
        return "movies/movie_page";
    }

    @PostMapping("/movies/{movie-id}/update")
    public String updateMovie(@PathVariable("movie-id") Long movieId,
                              @RequestParam("genre-id") Long genreId, MovieForm movie) {
        movieService.updateMovie(movieId, genreId, movie);
        return "redirect:/movies/" + movieId;
    }

    @GetMapping("/movies/{movie-id}/delete")
    public String updateMovie(@PathVariable("movie-id") Long movieId) {
        movieService.deleteMovie(movieId);
        return "redirect:/movies/";
    }
}
