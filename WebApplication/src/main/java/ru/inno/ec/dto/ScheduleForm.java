package ru.inno.ec.dto;

import lombok.*;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.sql.Time;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ScheduleForm {

    @NotNull
    @NotBlank
    private Date dateSession;

    @NotNull
    @NotBlank
    private String timeStart;

    @NotNull
    @NotBlank
    private String formMovie;

    @NotNull
    @NotBlank
    private Double price;
    
    @NotNull
    @NotBlank
    private String hall;
}
