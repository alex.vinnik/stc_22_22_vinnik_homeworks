package ru.inno.ec.dto;

import lombok.*;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MovieForm {
    @Size(min = 3, max = 50)
    @NotNull
    @NotBlank
    private String nameMovie;

    @NotNull
    @NotBlank
    private String year;

    private String country;

    @NotNull
    @NotBlank
    private Integer ageLimit;

    @NotNull
    @NotBlank
    private Integer duration;
}
