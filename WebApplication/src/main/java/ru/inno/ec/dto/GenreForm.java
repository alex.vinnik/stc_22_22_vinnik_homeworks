package ru.inno.ec.dto;

import lombok.*;
import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GenreForm {
    @NotNull
    @NotBlank
    private String nameGenre;
}
