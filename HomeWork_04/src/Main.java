import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static int sumNumbVInter(int leftG, int rightG) {
        int sumChisel = 0;

        if (rightG < leftG) {
            return -1;
        }

        for (int i = leftG; i <= rightG; i++) {
            sumChisel = sumChisel + i;
        }

        return sumChisel;
    }

    public static void outputEvenElementsArray(int[] userArr) {
        System.out.print("\nЧетные элементы массива: ");
        for (int i = 0; i < userArr.length; i++) {
            if (userArr[i] % 2 == 0) {
                System.out.print(userArr[i] + " ");
            }
        }
    }

    public static int toInt(int[] numbArr) {
        try {
            String tmpStr = "";

            for (int i = 0; i < numbArr.length; i++) {
                tmpStr += numbArr[i];
            }

            return Integer.parseInt(tmpStr);
        } catch (Exception ex) {
            return -1;
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Задание 1\n");
        System.out.print("Введите левую границу интервала: ");
        int leftG = scan.nextInt();
        System.out.print("Введите правую границу интервала: ");
        int rightG = scan.nextInt();
        System.out.println("Сумма чисел в интервале равна " + sumNumbVInter(leftG, rightG));
        System.out.println("\nЗадание 2 \n");
        System.out.print("Введите кол-во элементов массива: ");
        int number = scan.nextInt();
        int[] array = new int[number];

        for (int i = 0; i < number; i++) {
            System.out.print("Введите значение " + i + " элемента массива: ");
            array[i] = scan.nextInt();
        }

        outputEvenElementsArray(array);

        System.out.println();
        System.out.println("\nЗадание 3\n");
        int[] numb = {9, 8, 7, 2, 3};
        System.out.println("Результат: " + toInt(numb));
    }
}