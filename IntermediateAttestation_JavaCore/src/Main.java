public class Main {
    public static void main(String[] args) {
        ProductsRepostory productsRepository = new ProductsRepositoryFileBasedImpl("input.txt");
        System.out.println(productsRepository.findById(3));
        System.out.println(productsRepository.findById(10));
        System.out.println(productsRepository.findAllByTitleLike("Ин"));
        System.out.println(productsRepository.findAllByTitleLike("ИНд"));

        System.out.println(productsRepository.findAll());
        Product milk = productsRepository.findById(3);
        milk.setName("Edit 30");
        milk.setPrice(20.50);
        milk.setCountStock(40);

        productsRepository.update(milk);
        System.out.println(productsRepository.findAll());
    }
}