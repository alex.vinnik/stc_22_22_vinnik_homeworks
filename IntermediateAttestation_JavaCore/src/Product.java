import java.util.Objects;

public class Product {
    private Integer id;
    private String name;
    private Double price;
    private Integer countStock;

    public Product(Integer id, String name, Double price, Integer countStock) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.countStock = countStock;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setCountStock(Integer countStock) {
        this.countStock = countStock;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public Integer getCountStock() {
        return countStock;
    }

    @Override
    public String toString() {
        return id + "|" + name + "|" + price + "|" + countStock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return Objects.equals(id, product.id);
    }
}
