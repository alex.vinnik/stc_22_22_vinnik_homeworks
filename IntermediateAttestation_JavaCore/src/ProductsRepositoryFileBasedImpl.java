import java.io.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductsRepostory {
    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToUserMapper = currentUser -> {
        String[] parts = currentUser.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double price = Double.parseDouble(parts[2]);
        Integer countStock = Integer.parseInt(parts[3]);
        return new Product(id, name, price, countStock);
    };

    @Override
    public List<Product> findAll() {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToUserMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public Product findById(Integer id) {
        return findAll().stream().filter(product -> product.getId() == id)
                .findFirst().orElse(null);
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        List<Product> localProduct = findAll().stream().filter(product -> product.getName()
                .toLowerCase().contains(title.toLowerCase())).toList();
        localProduct = localProduct.isEmpty() ? null : localProduct;
        return localProduct;
    }

    @Override
    public void update(Product product) {
        List<Product> localProduct = findAll();
        try (FileWriter fileWriter = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

            for (Product tmpProduct : localProduct) {
                tmpProduct = tmpProduct.equals(product) ? product : tmpProduct;
                bufferedWriter.write(tmpProduct.toString());
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }
}
