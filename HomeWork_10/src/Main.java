import java.util.*;

public class Main {
    public static void main(String[] args) {
        String strTemp = "Hello Hello bye Hello bye Inno";

        assert wordOccursMostOften(strTemp).equals("Hello 3") : "!= Hello 3";
    }

    public static String wordOccursMostOften(String strUsers) {
        Map<String, Integer> mapCount = new HashMap<String, Integer>();
        String[] words = strUsers.split(" ");
        int maxValue = 0;
        String keyWord = "";

        for (String word : words) {
            if (mapCount.containsKey(word)) {
                mapCount.replace(word, mapCount.get(word) + 1);
            } else {
                mapCount.put(word, 1);
            }
        }

        for (Map.Entry<String, Integer> item : mapCount.entrySet()) {
            if (item.getValue() > maxValue) {
                maxValue = item.getValue();
                keyWord = item.getKey();
            }
        }

        return keyWord +" "+ mapCount.get(keyWord);
    }

}