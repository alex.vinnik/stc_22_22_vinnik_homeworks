import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CarsRepositoryFileBasedImpl implements CarsRepository {

    private final String fileName;

    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static Car parseCar(String line) {
        String[] cars = line.split("\\|");
        String numberCar = cars[0];
        String model = cars[1];
        String color = cars[2];
        Double mileage = Double.parseDouble(cars[3]);
        Double price = Double.parseDouble(cars[4]);
        return new Car(numberCar, model, color, mileage, price);
    }

    @Override
    public List<Car> findAll() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            return bufferedReader.lines().map(line -> parseCar(line)).collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
    }

    @Override
    public double averagePriceCamry() {
        return findAll().stream().filter(car -> car.getModel().toLowerCase().equals("camry"))
                .mapToDouble(Car::getPrice)
                .average()
                .getAsDouble();
    }

    @Override
    public String colorCarMinPrice() {
        return findAll().stream().min(Comparator.comparingDouble(Car::getPrice))
                .map(Car::getColor)
                .get();
    }

    @Override
    public List<String> numberCarBlackOrZero() {
        return findAll().stream()
                .filter(car -> car.getColor().toLowerCase().equals("black") || car.getMileage() == 0)
                .map(Car::getNumberCar)
                .toList();
    }

    @Override
    public long countUniqueModel() {
        return findAll().stream().filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                .map(Car::getModel)
                .distinct()
                .count();
    }
}
