import java.util.List;
import java.util.Optional;

public interface CarsRepository {
    List<Car> findAll();

    double averagePriceCamry();

    String colorCarMinPrice();

    List<String> numberCarBlackOrZero();

    long countUniqueModel();
}
