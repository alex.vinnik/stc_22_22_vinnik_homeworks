import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        CarsRepository usersRepository = new CarsRepositoryFileBasedImpl("input.txt");

        List<String> resultList = Arrays.asList("o001aa111", "o003aa111");
        double resultAvgPrice = 54666.67;
        String resultColorMin = "green";
        int resultCountUnqMod = 0;

        assert dRound2(usersRepository.averagePriceCamry()) == resultAvgPrice : "averagePriceCamry() != 54666.67";
        assert usersRepository.colorCarMinPrice().toLowerCase().equals(resultColorMin.toLowerCase()) : "colorCarMinPrice() != green";
        assert usersRepository.numberCarBlackOrZero().equals(resultList) : "numberCarBlackOrZero() != resultList";
        assert usersRepository.countUniqueModel() == resultCountUnqMod : "countUniqueModel() != 0";
    }

    public static double dRound2(double cr) {
        return BigDecimal.valueOf(cr).setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue();
    }
}