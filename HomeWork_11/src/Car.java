public class Car {
    private String numberCar;
    private String model;
    private String color;
    private Double mileage;
    private Double price;

    public Car(String numberCar, String model, String color, Double mileage, Double price) {
        this.numberCar = numberCar;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public String getNumberCar() {
        return numberCar;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public Double getMileage() {
        return mileage;
    }

    public Double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "CarsRepositoryFileBasedImpl{" +
                "numberCar='" + numberCar + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", price=" + price +
                '}';
    }
}
